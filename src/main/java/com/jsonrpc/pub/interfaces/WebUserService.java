package com.jsonrpc.pub.interfaces;

import java.util.List;

import com.jsonrpc.pub.dto.WebUser;
import com.jsonrpc.pub.exceptions.AlreadyExistsException;
import com.jsonrpc.pub.exceptions.NotFoundException;

public interface WebUserService {

	boolean createWebUser(WebUser user) throws AlreadyExistsException;
	boolean updateWebUser(WebUser user) throws NotFoundException;
	WebUser findOneById(long id) throws NotFoundException;
	WebUser findOneByUsername(String username) throws NotFoundException;
	List<WebUser> findAll() throws NotFoundException;
	
}