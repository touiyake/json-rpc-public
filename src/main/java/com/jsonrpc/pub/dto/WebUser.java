package com.jsonrpc.pub.dto;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class WebUser implements Serializable {

	private long id;
	private String username;
	private String password;
	private String email;
	private Address address;

}