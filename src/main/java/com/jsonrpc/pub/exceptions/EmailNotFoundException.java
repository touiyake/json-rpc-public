package com.jsonrpc.pub.exceptions;

public class EmailNotFoundException extends NotFoundException {

	public EmailNotFoundException() {
		super();
	}

	public EmailNotFoundException(String message, Throwable cause) {
		super(message, cause);
	}

	public EmailNotFoundException(String message) {
		super(message);
	}

	public EmailNotFoundException(Throwable cause) {
		super(cause);
	}

}
