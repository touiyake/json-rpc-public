package com.jsonrpc.pub.exceptions;

public class EmailAlreadyExistsException extends AlreadyExistsException {

	public EmailAlreadyExistsException() {
		super();
	}

	public EmailAlreadyExistsException(String message, Throwable throwable) {
		super(message, throwable);
	}

	public EmailAlreadyExistsException(String message) {
		super(message);
	}

	public EmailAlreadyExistsException(Throwable throwable) {
		super(throwable);
	}

}
