package com.jsonrpc.pub.exceptions;

public class AlreadyExistsException extends RuntimeException {

	public AlreadyExistsException() {
		super();
	}

	public AlreadyExistsException(String message, Throwable throwable) {
		super(message, throwable);
	}

	public AlreadyExistsException(String message) {
		super(message);
	}

	public AlreadyExistsException(Throwable throwable) {
		super(throwable);
	}

}