package com.jsonrpc.pub.exceptions;

public class UserAlreadyExistsException extends AlreadyExistsException {

	public UserAlreadyExistsException() {
		super();
	}

	public UserAlreadyExistsException(String message, Throwable throwable) {
		super(message, throwable);
	}

	public UserAlreadyExistsException(String message) {
		super(message);
	}

	public UserAlreadyExistsException(Throwable throwable) {
		super(throwable);
	}

}